# Project Euler Solutions in Python

This repository contains my solutions to [Project Euler](https://projecteuler.net/) problems, written in Python. Each solution is accompanied by a test case to ensure correctness and efficiency.

## Structure

- **/solutions**: Individual Python scripts for each problem.
- **/tests**: Test cases for each problem solution.
- **/utils**: Utility functions and classes used across different solutions.
- **/data**: Data files used in some of the problems.

## Getting Started

### Prerequisites

- Python 3.x

### Installation

Clone the repository and navigate to the folder:

```bash
git clone https://gitlab.com/vikibytes/project_euler_solutions.git
cd projet_euler_solutions
