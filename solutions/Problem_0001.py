# Sum of Multiples of 3 or 5

def sum_of_multiples_of_3_or_5(max_number):
    sum = 0
    for i in range(max_number):
        if i % 3 == 0 or i % 5 == 0:
            sum += i
    return sum


print(sum_of_multiples_of_3_or_5(1000))
